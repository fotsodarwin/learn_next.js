import React from 'react'

import {BsFacebook, BsTwitter, BsInstagram, BsLinkedin} from 'react-icons/bs';
const HeaderTop = () => {
  return (
    <div className='hidden border-b border-gray-200 sm:block'>
        <div className='container py-4'>
            <div className='flex items-center justify-between'>
                <div className='hidden gap-1 lg:flex'>
                    <div className='header_top__icon_wrapper'>
                        <BsFacebook/>
                    </div>
                    <div className='header_top__icon_wrapper'>
                        <BsTwitter/>
                    </div>
                    <div className='header_top__icon_wrapper'>
                        <BsInstagram/>
                    </div>
                    <div className='header_top__icon_wrapper'>
                        <BsLinkedin/>
                    </div>
                </div>
                <div className='text-gray-500 text-[12px]'>
                    <strong>Free shipping</strong> -
                    This week order over 55$
                </div>
                <div className='flex gap-4'>
                    <select name='currency' id='currency' className='text-gray-500 w-[70px] text-[12px]'>
                        <option value='USD'>USD ($)</option>
                        <option value='EUR'>EUR (£)</option>
                    </select>
                    <select name='language' id='language' className='text-gray-500 w-[80px] text-[12px]'>
                        <option value="English">English</option>
                        <option value="French">French</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
  )
}

export default HeaderTop