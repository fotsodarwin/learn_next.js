import React from 'react'
import Image from 'next/image'

const Testmonial = () => {
  return (
    <div >
        <div className='container py-16 grid-cols-2'>
            <h2 className='text-2xl font-medium pb-4'>Testimonials</h2>
            <div className='grid lg:grid-cols-[300px, 1fr] gap-4'>
                <div className='border border-gray-300 rounded-2xl grid place-items-center p-6 lg:p-0'>
                    <div className='text-center flex flex-col gap-1 items-center'>
                        <Image className='rounded-full inline-block' src='/user.jpg' width={80} height={80} alt='Profile' />
                        <h2 className='text-gray-500 font-black text-[20px]'>
                            Elsa Doe
                        </h2>
                        <p>
                            CEO & Founder Invision
                        </p>
                        <Image className='inline-block py-2' src='/quotes.svg' width={30} height={30} alt='Quote' />
                        <p className='max-w-[200px] text-gray-500'>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quos.
                        </p>
                    </div>
                </div>
                <div className='grid bg-red-600 bg-[url(/cta-banner.jpg)] bg-cover h-[500px] rounded-2xl  place-items-center'>
                    <div className=' bg-[#ffffffab] min-w-[270px] sm:min-w-[300px] md:min-w-[500px] rounded-xl py-8 sm:px-8 px-2 grid- place-items-center'>
                    <button className='bg-blackish text-white p-2 rounded-md'>
                         25% Discount
                    </button>
                    <h2 className='font-extrabold text-2xl text-[#272727]'>Summer Collection</h2>
                    <p className='text-gray-500'> 
                        Starting @ $20 <strong>Shop Now</strong>
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Testmonial