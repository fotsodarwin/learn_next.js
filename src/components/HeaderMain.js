import React from 'react'
import { BsSearch } from 'react-icons/bs'
import {BiUser} from 'react-icons/bi'
import {FiHeart} from 'react-icons/fi'
import { HiOutlineShoppingBag } from 'react-icons/hi'
const HeaderMain = () => {
  return (
    <div className='border-b border-gray-200 py-4 sm:block'>
        <div className='container flex justify-between items-center'>
            <div className='font-bold text-center text-4xl pb-4 text-blackish sm:pb-0'>Sick</div>

            <div className='relative w-full sm:w-[300px] md:w-[70%]'>
                <input type='text' placeholder='Enter any product name...'  className='border-gray-200 border px-2 rounded-lg w-full'/>
                <BsSearch className='absolute right-0 -top-1.5 mr-3 mt-3 text-gray-400'/>
            </div>

            <div className='hidden gap-4 lg:flex text-gray-500 text-[30px]'>
                <BiUser />

                <div className=' relative'>
                    <FiHeart />
                    <div className='bg-red-600 rounded-full absolute -top-1 right-0 w-[18px] h-[18px] text-[12px] text-white grid place-items-center translate-x-1 -translate-y-1'>0</div>
                </div>

                <div className='relative'>
                    <HiOutlineShoppingBag />
                    <div className='bg-red-600 rounded-full absolute -top-1 right-0 w-[18px] h-[18px] text-[12px] text-white grid place-items-center translate-x-1 -translate-y-1'>0</div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default HeaderMain