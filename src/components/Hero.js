"use client";

import React from 'react'
import Image from 'next/image'
import Slide from '@/components/Slide'
import Slider from 'react-slick'


const Hero = () => {
    let settings = {
        dots: true,
        infinite: true,
        autoplay: true,
        pauseOnHover:true,
        slideToShow: 1,
        slidesToScroll: 1,
    };

    const slideData = [
        {
            id:0,
            img: '/banner-1.jpg',
            title: 'Trending item',
            mainTitle: "Women's Latest Fashion Sale",
            price: '$20',
        },
        {
            id:1,
            img: '/banner-2.jpg',
            title: 'Trending Accessories',
            mainTitle: "MODERN SUNGLASSES",
            price: '$15',
        },
        {
            id:2,
            img: '/banner-3.jpg',
            title: 'Sale Offer',
            mainTitle: "New Fashion FSummer Sale",
            price: '$30',
        }
    ]
    
    return(
        <div>
            <div className='container pt-6 lg:pt-0 ' >
                <Slider {...settings}>
                    {slideData.map((item)=>{
                        return <Slide
                                key={item.id}
                                img = {item.img}
                                title={item.title}
                                mainTitle={item.mainTitle}
                                price={item.price}
                            />
                    })}
                </Slider>
            
            </div>
        </div>
    )
}

export default Hero