import React from 'react'
import ProductCard from './ProductCard'

const ProductsData = [
    {
        img:"/jacket-1.jpg",
        title:"Jacket",
        desc:"Men Yarn Fleece Full-Zip",
        price:"45.00",
        rating:4
    },
    {
        img:"/skirt-1.jpg",
        title:"Skirt",
        desc:"Black-Floral Wrap Midi Skirt",
        price:"55.00",
        rating:5
    },
    {
        img:"/party-wear-1.jpg",
        title:"Party Wear",
        desc:"Women's Party Wear Shoes",
        price:"25.00",
        rating:3
    },
    {
        img:"/shirt-1.jpg",
        title:"Shirt",
        desc:"Pure Garment Dyed Cotton Shirt",
        price:"45.00",
        rating:4
    },
    {
        img:"/sports-1.jpg",
        title:"Sports",
        desc:"Trekking and Running Shoes - Black",
        price:"58.00",
        rating:3
    },
    {
        img:"/watch-1.jpg",
        title:"Watches",
        desc:"Smart Watches Vital Plus",
        price:"100.00",
        rating:4
    },
    {
        img:"/watch-2.jpg",
        title:"Watches",
        desc:"Pocket Watch leather Pouch",
        price:"120.00",
        rating:4
    }
]
const NewProduct = () => {
  return (
    <div>
        <div className='container pt-16'>
            <h2 className='font-medium text-2xl pb-4'>New Products</h2>
            <div className='grid grid-cols-1 place-items-center sm:place-items-start sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-10 xl:gap-20 xl:gap-y-10'>
                {ProductsData.map((item, index )=>(
                    <ProductCard
                    
                    key={index} 
                    img={item.img}
                    title={item.title}
                    desc={item.desc}
                    price={item.price}
                    rating={item.rating}
                    />
                ))}
            </div>
        </div>
    </div>
  )
}

export default NewProduct