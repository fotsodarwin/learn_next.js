import React from 'react'
import Image from 'next/image'
import {AiFillStar, AiOutlineStar} from 'react-icons/ai'

const generateRating = (rating) => {
  switch (rating) {
    case 1:
      return <div className='flex gap-1 text-[20px] text-[#facc17]' >
        <AiFillStar />
      <AiOutlineStar />
      <AiOutlineStar />
      <AiOutlineStar />
      <AiOutlineStar />
        </div>
    case 2:
      return <div className='flex gap-1 text-[20px] text-[#FF9529]'>
        <AiFillStar />
        <AiFillStar />
      <AiOutlineStar />
      <AiOutlineStar />
      <AiOutlineStar />

      </div>  
    case 3:
      return <div className='flex gap-1 text-[20px] text-[#FF9529]'>
        <AiFillStar />
        <AiFillStar />
        <AiFillStar />
      <AiOutlineStar />
      <AiOutlineStar />
      </div>
    case 4:
      return <div className='flex gap-1 text-[20px] text-[#FF9529]'>
        <AiFillStar />
        <AiFillStar />
        <AiFillStar />
        <AiFillStar />
      <AiOutlineStar />
      </div>
    case 5:
      return <div className='flex gap-1 text-[20px] text-[#FF9529]'>
        <AiFillStar />
        <AiFillStar />
        <AiFillStar />
        <AiFillStar />
        <AiFillStar />
      </div>
  }
}
const ProductCard = ({img, title, desc, price, rating}) => {
  return (
    <div className='px-4 border border-gray-200 rounded-xl max-w-[400px]'>
      <div className=''>
        <Image src={img}
        className='w-full h-auto'
        width={200}
        alt={title}
        height={300} />
      </div>
      <div className='space-y-2 py-2'>
        <h2 className='text-accent font-medium uppercase'>{title}</h2>
        <p className='text-gray-500 '>{desc}</p>
        <p className=''>{generateRating(rating)}</p>
        <p className='font-bold flex gap-4'>
          ${price}
          <del className='text-gray font-normal'>
            ${parseInt(price) + 50}
          </del>
        </p>
      </div>
    </div>
  )
}

export default ProductCard